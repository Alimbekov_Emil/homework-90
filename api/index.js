const express = require("express");
const cors = require("cors");
const { nanoid } = require("nanoid");
const app = express();

require("express-ws")(app);

const port = 8000;

app.use(cors());

const activeConnections = {};
const pixels = [];

app.ws("/canvas", (ws, req) => {
  const id = nanoid();
  console.log("client connected , id=", id);

  activeConnections[id] = ws;
  ws.send(JSON.stringify({ type: "CONNECTED", message: pixels }));

  ws.on("message", (msg) => {
    const decoded = JSON.parse(msg);

    if (decoded.type === "CREATE_PIXELS") {
      const newPixels = pixels.concat(decoded.message);
      Object.keys(activeConnections).forEach((key) => {
        const connection = activeConnections[key];
        connection.send(
          JSON.stringify({
            type: "NEW_PIXELS",
            message: newPixels,
          })
        );
      });
    }

    ws.send(msg);
  });

  ws.on("close", () => {
    delete activeConnections[id];
    console.log("client disconected id=" + id);
  });
});

app.listen(port, () => {
  console.log(`Server startted on ${port} port!`);
});
